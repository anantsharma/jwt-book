app.service('contactService', function() {
    
    this.encode = function(input) {
        
        var output = '';
        
        // Input is array corresponding to $scope.contacts
        for (var i = 0; i < input.length; i++) {
            output += JSON.stringify(input[i]);
            if (i != input.length - 1) {
                output = output + '||';
            }
        }
        
        return output;
        
    };

    this.decode = function(input) {
        
        var output = [];
        
        // Input corresponds to stringified
        // of contact objects separated by ||
        // input = data.contacts
        
        var contacts = input.split('||');
        for (var i = 0; i < contacts.length; i++) {
            if (contacts[i] != "") {
                output.push(JSON.parse(contacts[i]));
            }
        }
        
        return output;
    };
    
});
