app.service('cassetteService', function(){
    var self = this;
    
    this.fill = function(amount, cassettes, contractType, screen) {
        
        /* Pre-Process Amount */
        amount = self.preProcessAmount(amount);
        
        /* Pre-Process Cassettes */
        self.preProcess(cassettes);
        
        /* Split pre selection
        based on screen */
        if (screen === 'rindent') {
            
            switch (contractType.toUpperCase()) {
                case 'TRANSACTION BASED': self.transactionSplit(amount, cassettes); break;
                case 'FIXED FEE': self.fixedFeeSplit(amount, cassettes); break;
                default: break;
            }
            
        } else {
            switch (contractType.toUpperCase()) {
                case 'TRANSACTION BASED': self.transactionSplit(amount, cassettes); break;
                case 'FIXED FEE': self.fixedFeeSplit(amount, cassettes); break;
                default: break;
            }
        }
    };
    
    this.preProcessAmount = function(amount) {
        
        amount = Number(amount);
        
        if (isNaN(amount)) {
            amount = 0;
        }
        
        amount = parseInt(amount, 10);
        
        return amount;
    };
    
    this.preProcess = function(cassettes) {
        
        /* Typecast values to number */
        cassettes.forEach(function(cassette) {
            
            /* Enumerate through each key of object */
            for (var key in cassette) {
                
                cassette[key] = Number(cassette[key]);
                
                if (isNaN(cassette[key])) {
                    cassette[key] = 0;
                }
                
            }
            
        });
        
        /* Sort cassettes by denomination */
        cassettes.sort(function(a, b) {
            return a.denomination - b.denomination;
        });
        
        /* Additional per cassette computation */
        cassettes.forEach(function(c) {
            
            /* Current Notes */
            if (!c.hasOwnProperty('currentNotes')) {
                c.currentNotes = c.currentBalance / c.denomination;
            }
            
            if (!isFinite(c.currentNotes)) {
                c.currentNotes = 0;
            }
            
        });
        
    };
    
    this.transactionSplit = function(amount, cassettes) {
        
        cassettes.forEach(function(c) {
            
            /* Check for amount to be splitted */
            if (amount <= 0) {
                c.loadingNotes = 0;
                return;
            }
            
            /* Spare Capacity */
            var remainingCapacity = c.capacity - c.currentNotes;
            
            /* Loading Based on assumed amount */
            var loadingBasedOnAssumedAmount = 
                remainingCapacity * c.denomination;
            if (loadingBasedOnAssumedAmount > amount) {
                loadingBasedOnAssumedAmount = amount;
            }
            
            /* Number of Notes */
            var numberOfNotes = loadingBasedOnAssumedAmount / 
                c.denomination;
            
            /* Round off to nearest 100 
            on upper limit */
            numberOfNotes = Math.ceil(numberOfNotes / 100) * 100;
            
            /* Check numberOfNotes with spare capacity */
            if (numberOfNotes > remainingCapacity) {
                numberOfNotes = Math.floor(remainingCapacity / 100) * 100;
            }
            
            /* Loading Amount */
            var loadingAmount = numberOfNotes * c.denomination;
            
            /* Round off to multiple of 50K or 100K */
            loadingAmount = Math.round(c.loadingAmount / 100000) * 100000;
            
            /* Perform Final Check */
            if ((loadingAmount / c.denomination) > remainingCapacity) {
                
                c.loadingAmount = Math.floor(
                    (numberOfNotes * c.denomination) / 50000) * 50000;
            }
            
            c.loadingNotes = loadingAmount / c.denomination;
            c.finalNotes = angular.copy(c.loadingNotes);
            
            amount -= c.loadingAmount;
            
        });
        
    };
    
    this.fixedFeeSplit = function(amount, cassettes) {
        // amount is the forecasted amount after rules
        // Initialize atm capacity with 0
        let atmMaxCapacity = 0;
        // Initialize atm value with 0
        let atmValue = 0;
        
        // Get the maximum atm cash capacity
        angular.forEach(cassettes, function(cassette, i){
            atmMaxCapacity += cassette.denomination * (cassette.capacity || 0);
        });
        
        // If atm has no capacity
        if(atmMaxCapacity == 0){
            return;
        }
        
        angular.forEach(cassettes, function(c, i){
            
            // If cassette has no capacity
            if (c.capacity === 0) {
                c.currentNotes = 0;
            }
            
            c.remainingNotes = c.capacity - c.currentNotes;
            c.cassetteShare = (c.denomination * c.capacity) / atmMaxCapacity;
            c.loadingAmount = c.cassetteShare * amount;
            c.number_of_notes = c.loadingAmount / c.denomination;
            
            // Round off to 100 Notes
            c.loadingNotes = Math.ceil(c.number_of_notes/100)*100;
            // Comparison to Free Capacity
            if(c.loadingNotes > c.remainingNotes){
                c.loadingNotes = Math.floor(c.remainingNotes/100)*100;
            }
            
            c.value = c.loadingNotes * c.denomination;
            
            // Check multiple of 50K or 100K
            if(!mod(c.value, 50000)){
                // If not multiple of 50K round to multiple of 100K
                c.value = Math.round(c.value/100000) * 100000;
            }
            
            // Final Check
            if(c.value/c.denomination > c.remainingNotes){
                c.value = Math.floor((c.loadingNotes * c.denomination)/100000) * 100000;
            }
            
            c.loadingNotes = c.value/c.denomination;
            c.finalNotes = angular.copy(c.loadingNotes);
            c.finalValue = c.finalNotes * c.denomination;
        });
    };
    
    this.rIndentFixedFeeSplit = function(amount, cassettes) {
        
        // Initialize atm capacity with 0
        let atmMaxCapacity = 0;
        
        // Initialize atm value with 0
        let atmValue = 0;
        
        // Get the maximum atm cash capacity
        angular.forEach(cassettes, function(cassette, i){
            atmMaxCapacity += cassette.denomination * cassette.capacity;
        });
        
        // If atm has no capacity
        if(atmMaxCapacity == 0){
            return;
        }
        
        angular.forEach(cassettes, function(c, i){
            c.remainingNotes = c.capacity - c.currentNotes;
            c.cassetteShare = (c.denomination * c.capacity)/atmMaxCapacity;
            c.loadingAmount = c.cassetteShare * amount;
            c.number_of_notes = c.loadingAmount/c.denomination;
            
            // Round off to 100 Notes
            c.loadingNotes = Math.ceil(c.number_of_notes/100)*100;
            // Comparison to Free Capacity
            if(c.loadingNotes > c.remainingNotes){
                c.loadingNotes = Math.floor(c.remainingNotes/100)*100;
            }
            
            c.value = c.loadingNotes * c.denomination;
            
            // Check multiple of 50K or 100K
            if(!mod(c.value, 50000)){
                // If not multiple of 50K round to multiple of 100K
                c.value = Math.round(c.value/100000) * 100000;
            }
            
            // Final Check
            if(c.value/c.denomination > c.remainingNotes){
                c.value = Math.floor((c.loadingNotes * c.denomination)/100000) * 100000;
            }
            
            c.loadingNotes = c.value/c.denomination;
            c.finalNotes = angular.copy(c.loadingNotes);
            c.finalValue = c.finalNotes * c.denomination;
        });
    };
    
});

function mod(a, b) {
    if ((a % b) == 0) {
        return true;
    }
    return false;
}