app.controller('userViewController', function($scope, $location, userService){
    
    $scope.pageHeader = 'User Master';
    
    /* Define table headers */
    $scope.headers = [{
        title: 'First Name',
        key: 'fname',
        sortable: true,
        type: 'string'
    }, {
        title: 'Last Name',
        key: 'lname',
        sortable: true,
        type: 'string'
    }, {
        title: 'ID',
        key: 'user_id',
        sortable: true,
        type: 'string'
    }, {
        title: 'Department',
        key: 'department',
        sortable: true,
        type: 'string'
    }, {
        title: 'Role',
        key: 'role',
        sortable: true,
        type: 'string'
    }, {
        title: 'Edit',
        key: 'edit'
    }, {
        title: 'Status',
        key: 'active'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        userService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    
    $scope.fetch();
    
    /* Function to redirect to
    add/edit mode */
    $scope.redirect = function(id) {
        $location.path('/user/' + id);
    };
    
    /* Enable/Disable */
    $scope.toggle = function(obj) {
        var o = {};
        o["fc/id"] = obj["fc/id"];
        o.user_id = obj["user_id"];
        o.active = obj.active;
        
        userService.put(o, 'toggle', function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
        });
        
    };
    
});

app.controller('userInputController', function($scope, $location, bankService, 
    $routeParams, locationService, userService, toasterService) {
    
    /* Identify Page */
    $scope.screen = 'user';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {};
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        userService.detail($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            try {
                
                response.banks = JSON.parse(response.banks);
                response.states = JSON.parse(response.states);
                response.screens = JSON.parse(response.screens);
                
            } catch (e) {
                console.trace(e);
            }
            
            $scope.o = response;
            
        });
        
    }
    
    /* Location Operations */
    $scope.location = {
        get: {}
    };
    /* Get State List */
    $scope.location.get.state = function() {
        locationService.unique.state('INDIA', function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.state = response;
            
        });
    };
    $scope.location.get.state();
    
    /* Bank Operations */
    $scope.bank = {
        get: {}
    };
    /* Bank List */
    $scope.bank.get.list = function() {
        bankService.unique(function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.bank.list = response;
        });
    };
    $scope.bank.get.list();
    
    /* Screen Operations */
    $scope.screen = {
        get: {}
    };
    /* Screen List */
    $scope.screen.get.list = function() {
        
        $scope.screen.list = [
            "Bank Master",
            "Nodal Branch Master",
            "ATM Master",
            "CIT Master",
            "CRA Location Master",
            "Common Master",
            "Cash Planner",
            "Project Master",
            "Rules Master",
            "Uploader Screen",
            "Indent Screen",
            "User Management",
            "Location Master"
        ];
        
    };
    $scope.screen.get.list();
    
    /* User Operations */
    $scope.user = {};
    
    /* Save User Details */
    $scope.user.save = function() {
        
        userService.put($scope.o, $scope.mode, function(err, response) {
            
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'User Master',
            };
            
            if (err) {
                
                toastObj.type = 'error';
                
                if (err.hasOwnProperty('error')) {
                    toastObj.body = err.error;
                } else if (err.hasOwnProperty('status')) {
                    toastObj.body = err.msg;
                } else {
                    toastObj.body = err.toString();
                }
                
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.user.cancel();
            
        });
        
    };
    
    /* Cancel User Add / Edit */
    $scope.user.cancel = function() {
        $location.path('/user');
    };
    
});
