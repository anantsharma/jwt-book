app.service('userService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/user', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get details of user
    corresponding to fc/id sent as id */
    this.detail = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/user', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of users */
    this.unique = function(params, cb) {
        
        $http.post(CONFIG.host + '/api/v1/user/unique', params).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, mode, cb) {
        
        $http.put(CONFIG.host + '/api/v1/user', { data: data, mode: mode }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
    /** Change Password */
    this.changePassword = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/user/change', { data: data }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
});
