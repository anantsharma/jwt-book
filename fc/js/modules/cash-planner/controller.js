app.controller('plannerController', function($scope, $location, plannerService){
    
    $scope.pageHeader = 'Cash Planner';
    
    /* Initialize Variables */
    var checkedATMBucket = [];
    
    /* Action Buttons */
    $scope.buttons = [{
        name: 'EOD',
        action: function() {
            
            checkedATMBucket = [];
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    if (atm.isChecked) {
                        
                        /* Set status */
                        atm.planStatus = 'EOD';
                        
                        checkedATMBucket.push(atm);
                        
                    }
                    
                });
                
            });
            
            /* Push to DB */
            $scope.save();
            
        }
    }, {
        name: 'Finalize Plan',
        action: function() {
            
            checkedATMBucket = [];
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    if (atm.isChecked) {
                        
                        /* Set status */
                        atm.planStatus = 'Finalize Plan';
                        
                        checkedATMBucket.push(atm);
                        
                    }
                    
                });
                
            });
            
            /* Push to DB */
            $scope.save();
            
        }
    }, {
        name: 'Fully Load',
        action: function() {
            
            checkedATMBucket = [];
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    if (atm.isChecked) {
                        
                        var _atm = angular.copy(atm);
                        
                        /* Set status */
                        _atm.planStatus = 'Fully Load';
                        
                        /* Reset modified amount */
                        _atm.currentData.modifiedAmount = 0;
                        
                        /* Set notes */
                        angular.forEach(_atm.cassettes, function(c) {
                            
                            var spareCapacity = (c.capacity - c.currentNotes) * c.denomination;
                            
                            c.finalNotes = (Math.floor(spareCapacity / 50000, 10) * 50000) / c.denomination;
                            
                            _atm.currentData.modifiedAmount += c.finalNotes * c.denomination;
                            
                        });
                        
                        checkedATMBucket.push(_atm);
                        
                    }
                    
                });
                
            });
            
            /* Push to DB */
            $scope.save();
            
        }
    }, {
        name: 'No Load',
        action: function() {
            
            checkedATMBucket = [];
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    if (atm.isChecked) {
                        
                        var _atm = angular.copy(atm);
                        
                        /* Set status */
                        _atm.planStatus = 'No Load';
                        
                        /* Reset modified amount */
                        _atm.currentData.modifiedAmount = 0;
                        
                        /* Set notes */
                        angular.forEach(_atm.cassettes, function(c) {
                            
                            c.finalNotes = 0;
                            
                        });
                        
                        checkedATMBucket.push(_atm);
                        
                    }
                    
                });
                
            });
            
            /* Push to DB */
            $scope.save();
            
        }
    }, {
        name: 'Restore',
        action: function() {
            
            checkedATMBucket = [];
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    if (atm.isChecked) {
                        
                        /* Set status */
                        atm.planStatus = 'Not Planned';
                        
                        /* Set Amount */
                        atm.currentData.modifiedAmount = atm.currentData.loadingAmountAfterRules;
                        
                        /* Set notes */
                        angular.forEach(atm.cassettes, function(c) {
                            
                            c.finalNotes = c.loadingNotes;
                            
                        });
                        
                        checkedATMBucket.push(atm);
                        
                    }
                    
                });
                
            });
            
            /* Push to DB */
            $scope.save();
            
        }
    }, {
        name: 'Expand All',
        action: function() {
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    /* Set cassette expand */
                    atm.cassettesExpand = true;
                });
                
            });
            
        }
    }, {
        name: 'Collapse All',
        action: function() {
            
            /* Iterate for both normal
            and exception atms list */
            angular.forEach($scope.atms, function(list) {
                
                /* Iterate through each atm
                in the list */
                angular.forEach(list, function(atm) {
                    
                    /* Unset cassette expand */
                    atm.cassettesExpand = false;
                });
                
            });
            
        }
    }];
    
    /* Filter Params */
    $scope.params = {};
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        
        delete $scope.atms;
        
        plannerService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.atms = {
                normal: [],
                exceptions: []
            };
            
            try {
                $scope.ts = Number(response.ts);
            } catch (e) {}
            
            response.list.forEach(function(obj) {
                obj.cassettesExpand = false;
                
                /** Accuracy processing */
                obj.accuracy = Number(obj.accuracy);
                if (isNaN(obj.accuracy)) {
                    obj.accuracy = '';
                } else {
                    obj.accuracy = (obj.accuracy * 100).toFixed(2);
                }
                
                /** Current Data Processing */
                if (obj.hasOwnProperty('currentData')) {
                    obj.currentData.loadingAmountAfterRules = parseInt(obj.currentData.loadingAmountAfterRules, 10);
                    obj.currentData.modifiedAmount = parseInt(obj.currentData.modifiedAmount, 10);
                    
                    obj.currentData.openingBalance = 0;
                    angular.forEach(obj.cassettes, function(c) {
                        obj.currentData.openingBalance += Number(c.currentBalance);
                    });
                }
                
                if (obj.planToday) {
                    $scope.atms.normal.push(obj);
                } else {
                    $scope.atms.exceptions.push(obj);
                }
                
            });
            
        });
    };
    
    $scope.fetch();
    
    /* Update ATM Record */
    $scope.save = function() {
        
        plannerService.put(checkedATMBucket, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
        });
        
    };
    
});

app.directive('refineContainer', function() {
    return {
        restrict: 'E',
        templateUrl: 'fc/components/planner/refine.html',
        scope: {
            o: '=params',
            fetch: '&'
        },
        controller: function($scope, $rootScope, nodalService,
        authService, locationService) {
            
            /* Bank Operations */
            $scope.bank = {};
            
            /* Location Operations */
            $scope.location = {
                get: {}
            };
            /* Get City List */
            $scope.location.get.city = function(country, state) {
                
                $scope.location.city = [];
                
                locationService.unique.city(country, state, function(err, response) {
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.location.city = response;
                });
            };
            
            /* Nodal Branch Operations */
            $scope.nodalBranch = {
                get: {}
            };
            /* Nodal Branch List */
            $scope.nodalBranch.get.list = function() {
                
                $scope.nodalBranch.list = [];
                
                if (!angular.isDefined($scope.o.bank) || $scope.o.bank == '') {
                    return;
                }
                
                /** Add bank filter */
                var params = {
                    bank: [$scope.o.bank]
                };
                
                /** Add state filter if state is present */
                if ($scope.o.state && angular.isDefined($scope.o.state) && $scope.o.state !== '') {
                    params.state = [$scope.o.state];
                }
                /** Add city filter if city is present */
                if ($scope.o.city && angular.isDefined($scope.o.city) && $scope.o.city !== '') {
                    params.city = [$scope.o.city];
                }
                
                nodalService.unique(params, function(err, response) {
                    
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.nodalBranch.list = response;
                });
            };
            
            /* Function to populate
            permission based values */
            $scope.evaluateToken = function() {
                
                /* Get Token */
                var token = authService.parseJwt();
                
                /* Assign Permissions */
                $scope.bank.list = token.permissions.banks;
                $scope.location.state = token.permissions.states;
                
                /* Insert Empty Values */
                $scope.bank.list.push('');
                $scope.location.state.push('');
                
            };
            $scope.evaluateToken();
            
            $rootScope.$on('authService.token.save', function(event, data) {
                $scope.evaluateToken();
            });
            
            /* Watchers */
            $scope.$watch('o.state', function(n, o) {
                if (n === o) {
                    return;
                }
                
                /* Change city if state was defined
                and is changed now */
                if (o !== undefined) {
                    $scope.o.city = null;
                }
                
                $scope.location.get.city('INDIA', n);
                $scope.nodalBranch.get.list();
            });
            
            $scope.$watch('o.city', function(n, o) {
                if (n === o) {
                    return;
                }
                
                $scope.nodalBranch.get.list();
            });
            
            $scope.$watch('o.bank', function(n, o) {
                if (n === o) {
                    return;
                }
                
                /* Change state if bank was defined
                and is changed now */
                if (o !== undefined) {
                    $scope.o.nodal_branch = null;
                }
        
                $scope.nodalBranch.get.list();
            });
            
        }
    };
});

app.directive('plannerTable', function() {
    return {
        restrict: 'E',
        templateUrl: 'fc/components/planner/planner-table.html',
        scope: {
            list: '=',
            ts: '='
        },
        controller: function($scope, $interval) {
            
            /* Change pastData based
            on arrows in planner table */
            $scope.prevDate = function() {
                var i = $scope.activeDateRange.indexOf($scope.activeDate);
                if (i === 0) {
                    return;
                }
                $scope.activeDate = $scope.activeDateRange[i - 1];
                $scope.updateActiveData();
            };
            $scope.nextDate = function() {
                var i = $scope.activeDateRange.indexOf($scope.activeDate);
                if (i === ($scope.activeDateRange.length - 1)) {
                    return;
                }
                $scope.activeDate = $scope.activeDateRange[i + 1];
                $scope.updateActiveData();
            };
            $scope.updateActiveData = function() {
                
                /* Iterate through each atm */
                $scope.list.forEach(function(atm) {
                    
                    /** Nullify atm.activeData */
                    atm.activeData = {};
                    
                    /* Iterate through each entry
                    in pastData */
                    atm.pastData.forEach(function(obj) {
                        
                        /* Check for date equality */
                        if (obj.date === $scope.activeDate) {
                            
                            /* If date equality is present, 
                            update it to show */
                            atm.activeData = obj;
                        }
                    });
                    
                });
                
            };
            
            /* Set Current Date */
            $scope.currentDate = moment($scope.ts).format("DD-MM-YYYY");
            
            /* Set active date and present data */
            try {
                $scope.activeDateRange = makeDateRange(7, $scope.ts);
                $scope.activeDate = $scope.activeDateRange[$scope.activeDateRange.length - 1];
                $scope.updateActiveData();
            } catch (e) {
                console.trace(e);
            }
            
            /* Master Checkbox */
            $scope.masterCheckboxChange = function() {
                angular.forEach($scope.list, function(atm) {
                    atm.isChecked = $scope.masterCheckbox.isChecked;
                });
            };
            
        }
    };
});

app.directive('plannerTableRow1', function() {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'fc/components/planner/planner-table-row-1.html',
        scope: {
            atm: '='
        },
        controller: function($scope, cassetteService) {
            
            /* Fill cassette by splitting amount */
            $scope.splitAndFill = function() {
                cassetteService.fill($scope.atm.currentData.modifiedAmount, 
                $scope.atm.cassettes, $scope.atm.contractType);
            };
            $scope.splitAndFill();
            
        }
    };
});

app.directive('plannerTableRow2', function() {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'fc/components/planner/planner-table-row-2.html',
        controller: function($scope) {
            
        }
    };
});

function makeDateRange(n, d) {
    
    try {
        d = Number(d);
    } catch (e) {
        console.trace(e);
    }
    
    var arr = [];
    
    /* Make date range by 
    recursively calling 
    moment date fn */
    
    for (var i = n; i > 0; i--) {
        var k = moment(d).subtract(i, 'days').format("DD-MM-YYYY");
        arr.push(k);
    }
    
    return arr;
}
