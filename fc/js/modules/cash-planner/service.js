app.service('plannerService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        var p = angular.copy(params);
        
        if (typeof(p) === 'object' && !Array.isArray(p)) {
            
            if (p.hasOwnProperty('accuracy')) {
                for (var key in p.accuracy) {
                    try {
                        if (p.accuracy[key].length > 0) {
                            p.accuracy[key] = Number(p.accuracy[key]) / 100;
                        }
                    } catch (e) {
                        console.trace(e);
                    }
                }
            }
            
            $http.post(CONFIG.host + '/api/v1/planner', p).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/planner', data).then(function(response) {
            cb(null, response);
        });
        
    };
    
});
