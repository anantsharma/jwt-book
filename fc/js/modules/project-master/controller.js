app.controller('projectViewController', function($scope, $location, projectService){
    
    $scope.pageHeader = 'Project Master';
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Project Name',
        key: 'name',
        sortable: true,
        type: 'string'
    },{
        title: 'Bank',
        key: 'bank',
        sortable: true,
        type: 'string'
    },{
        title: 'Start Date',
        key: 'activeContractStartDate',
        sortable: false,
        type: 'date'
    },{
        title: 'End Date',
        key: 'activeContractEndDate',
        sortable: false,
        type: 'date'
    },{
        title: 'Edit',
        key: 'edit'
    },{
        title: 'Status',
        key: 'active'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        projectService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    
    $scope.fetch();
    
    /* Function to redirect to
    add/edit mode */
    $scope.redirect = function(id) {
        $location.path('/project/' + id);
    };
    
    /* Enable/Disable */
    $scope.toggle = function(obj) {
        var o = {};
        o["fc/id"] = obj["fc/id"];
        o.active = obj.active;
        
        projectService.put(o, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
        });
        
    };
    
});

app.controller('projectInputController', function($scope, $location, 
    bankService, $routeParams, projectService, toasterService){
    
    /* Identify Page */
    $scope.screen = 'project';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {
        contracts: []
    };
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        projectService.detail($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
            
            var contracts = $scope.o.contracts.split('||');
            $scope.o.contracts = [];
            contracts.forEach(function(c) {
                $scope.o.contracts.push(JSON.parse(c));
            });
            
        });
        
    }
    
    /* Bank Operations */
    $scope.bank = {
        get: {}
    };
    
    /* Bank List */
    $scope.bank.get.list = function() {
        bankService.unique(function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.bank.list = response;
        });
    };
    $scope.bank.get.list();
    
    /* Contract Operations */
    $scope.contract = {
        showBtn: true
    };
    
    /* Add Contract */
    $scope.contract.add = function() {
        
        $scope.o.contracts.unshift({});
        $scope.contract.showBtn = false;
        
    };
    
    /* Remove Contract */
    $scope.contract.remove = function(index) {
        
        $scope.o.contracts.splice(index, 1);
    };
    
    /* If mode is add, add one blank contact */
    if ($scope.mode === 'add') {
        $scope.contract.add();
    }
    
    /* Project Operations */
    $scope.project = {};
    
    /* Save Project Details */
    $scope.project.save = function() {
        
        $scope.errors = [];
        
        projectService.put($scope.o, function(err, response) {
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'Project Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.project.cancel();
        });
    };
    
    /* Cancel Project Add / Edit */
    $scope.project.cancel = function() {
        $location.path('/project');
    };
    
});
