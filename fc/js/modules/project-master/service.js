app.service('projectService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/project', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get details of project
    corresponding to fc/id sent as id */
    this.detail = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/project', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of projects */
    this.unique = function(bank, cb) {
        
        var params = {};
        
        if (angular.isDefined(bank)) {
            params.bank = bank;
        }
        
        $http.post(CONFIG.host + '/api/v1/project/unique', params).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/project', { data: data }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
});
