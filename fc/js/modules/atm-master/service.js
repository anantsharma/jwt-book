app.service('atmService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/atm', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get details of atm
    corresponding to fc/id sent as id */
    this.detail = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/atm', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of items */
    this.unique = function(params, cb) {
        
        if (typeof(params) !== 'object' || Array.isArray(params)) {
            params = {};
        }
        
        $http.post(CONFIG.host + '/api/v1/atm/unique', params).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        var d = JSON.parse(JSON.stringify(data));
        
        /** Perform check for toggle operation
         */
        if (!(Object.keys(d).length === 2 && 
        data.hasOwnProperty('fc/id') && 
        data.hasOwnProperty('active'))) {
            
            /** Process Cassette Info */
            for (var i = 1; i < 5; i++) {
                delete d[`cassette_${i}_denomination`];
                d[`cassette_${i}_note_capacity`] = Number(d[`cassette_${i}_note_capacity`]) || 0;
            }
            
        }
        
        $http.put(CONFIG.host + '/api/v1/atm', { data: d }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
});
