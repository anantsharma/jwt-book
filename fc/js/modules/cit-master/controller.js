app.controller('citViewController', function($scope, $location, citService){
    
    $scope.pageHeader = 'CIT Master';
    
    /* Define table headers */
    $scope.headers = [{
        title: 'CIT Name',
        key: 'name',
        sortable: true,
        type: 'string'
    },{
        title: 'CIT Short Name',
        key: 'shortName',
        sortable: true,
        type: 'string'
    },{
        title: 'City',
        key: 'city',
        sortable: true,
        type: 'string'
    },{
        title: 'State',
        key: 'state',
        sortable: true,
        type: 'string'
    },{
        title: 'Pincode',
        key: 'pincode',
        sortable: true,
        type: 'number'
    },{
        title: 'Edit',
        key: 'edit'
    },{
        title: 'Status',
        key: 'active'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        citService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    
    $scope.fetch();
    
    /* Function to redirect to
    add/edit mode */
    $scope.redirect = function(id) {
        $location.path('/cit/' + id);
    };
    
    /* Enable/Disable */
    $scope.toggle = function(obj) {
        var o = {};
        o["fc/id"] = obj["fc/id"];
        o.active = obj.active;
        
        citService.put(o, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
        });
        
    };
    
});

app.controller('citInputController', function($scope, $location, 
    $routeParams, locationService, citService, toasterService){
    
    /* Identify Page */
    $scope.screen = 'cit';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {
        contacts: []
    };
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        citService.detail($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
            
            var contacts = $scope.o.contacts.split('||');
            $scope.o.contacts = [];
            contacts.forEach(function(c) {
                c = $scope.o.contacts.push(JSON.parse(c));
            });
            
        });
        
    }
    
    /* Location Operations */
    $scope.location = {
        get: {}
    };
    
    /* Get Country List */
    $scope.location.get.country = function() {
        locationService.unique.country(function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.country = response;
        });
    };
    $scope.location.get.country();
    
    /* Get State List */
    $scope.location.get.state = function(country) {
        locationService.unique.state(country, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.state = response;
        });
    };
    
    /* Get City List */
    $scope.location.get.city = function(country, state) {
        locationService.unique.city(country, state, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.city = response;
        });
    };
    
    /* Contact Operations */
    $scope.contact = {};
    
    /* Add Contact */
    $scope.contact.add = function() {
        
        $scope.o.contacts.push({});
        
    };
    
    /* Remove Contact */
    $scope.contact.remove = function(index) {
        
        $scope.o.contacts.splice(index, 1);
    };
    
    /* If mode is add, add one blank contact */
    if ($scope.mode === 'add') {
        $scope.contact.add();
    }
    
    /* CIT Operations */
    $scope.cit = {};
    
    /* Save Branch Details */
    $scope.cit.save = function() {
        
        $scope.errors = [];
        
        citService.put($scope.o, function(err, response) {
            
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'CIT Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.cit.cancel();
            
        });
    };
    
    /* Cancel Branch Add / Edit */
    $scope.cit.cancel = function() {
        $location.path('/cit');
    };
    
    /* Watchers */
    $scope.$watch('o.country', function(n, o) {
        if (n === o) {
            return;
        }
        
        /* Change state if country was defined
        and is changed now */
        if (o !== undefined) {
            $scope.o.state = null;
        }

        $scope.location.get.state(n);
    });
    
    $scope.$watch('o.state', function(n, o) {
        if (n === o) {
            return;
        }
        
        /* Change city if state was defined
        and is changed now */
        if (o !== undefined) {
            $scope.o.city = null;
        }
        
        $scope.location.get.city($scope.o.country, n);
    });
    
});
