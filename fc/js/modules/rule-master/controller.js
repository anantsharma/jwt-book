app.controller('ruleViewController', function($scope, $location, ruleService) {
    
    $scope.pageHeader = 'Rules Master';
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Rule Name',
        key: 'rname',
        sortable: false,
        type: 'string'
    },,{
        title: 'Start Date',
        key: 'start_date',
        sortable: false,
        type: 'date',
        format: 'DD-MM-YYYY'
    },{
        title: 'End Date',
        key: 'end_date',
        sortable: false,
        type: 'date',
        format: 'DD-MM-YYYY'
    },{
        title: 'View',
        key: 'view'
    },{
        title: 'Status',
        key: 'active'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        ruleService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    
    $scope.fetch();
    
    /* Function to redirect to
    add/edit mode */
    $scope.redirect = function(id) {
        $location.path('/rule/' + id);
    };
    
    /* Enable/Disable */
    $scope.toggle = function(obj) {
        var o = {
            "fc/id": obj["fc/id"],
            active: obj.active
        };
        
        ruleService.put(o, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
        });
        
    };
    
});

app.controller('ruleInputController', function($scope, $location, $routeParams, 
    bankService, atmService, nodalService, ruleService, 
    citService, toasterService) {
    
    /** Initialize blank obj
     * to contain information
     */
    $scope.o = {
        aggs: {
            banks: [],
            nodalBranches: [],
            cras: [],
            atms: []
        }
    };
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Rule Details */
        ruleService.detail($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
            
        });
        
    }
    
    /* Bank Operations */
    $scope.bank = {
        get: {}
    };
    /* Bank List */
    $scope.bank.get.list = function() {
        bankService.unique(function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.bank.list = response;
        });
    };
    $scope.bank.get.list();
    
    /* Nodal Branch Operations */
    $scope.nodal = {
        get: {}
    };
    /* Nodal Branch List */
    $scope.nodal.get.list = function() {
        
        var params = {
            bank: $scope.o.aggs.banks
        };
        
        nodalService.unique(params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.nodal.list = response;
        });
    };
    
    /* CRA Operations */
    $scope.cra = {
        get: {}
    };
    /* CRA List */
    $scope.cra.get.list = function() {
        citService.unique(function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.cra.list = response;
        });
    };
    $scope.cra.get.list();
    
    /* ATM Operations */
    $scope.atm = {
        get: {}
    };
    /* ATM List */
    $scope.atm.get.list = function() {
        
        if (!($scope.o.aggs.banks.length 
            && $scope.o.aggs.nodalBranches.length
            && $scope.o.aggs.cras.length)) {
                $scope.atm.list = [];
                $scope.o.aggs.atms = [];
                return;
        }
        
        var params = {
            bank: $scope.o.aggs.banks,
            nodal: $scope.o.aggs.nodalBranches,
            cra: $scope.o.aggs.cras
        };
        
        atmService.unique(params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.atm.list = response;
        });
    };
    
    /* Rule Operations */
    $scope.rule = {};
    
    /* Save Rule Details */
    $scope.rule.save = function() {
        
        ruleService.put($scope.o, function(err, response) {
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'Rules Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            toastObj.body = 'Record added successfully';
            
            toasterService.pop(toastObj);
            $scope.rule.cancel();
        });
        
    };
    
    /* Cancel Rule Add */
    $scope.rule.cancel = function() {
        $location.path('/rule');
    };
    
    /* Watchers */
    $scope.$watch('o.aggs.banks', function(n, o) {
        if (n === o) {
            return;
        }

        $scope.nodal.get.list();
        $scope.atm.get.list();
    });
    
    $scope.$watch('o.aggs.nodalBranches', function(n, o) {
        if (n === o) {
            return;
        }
        
        $scope.atm.get.list();
    });
    
    $scope.$watch('o.aggs.cras', function(n, o) {
        if (n === o) {
            return;
        }
        
        $scope.atm.get.list();
    });
    
});