app.service('locationService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/location', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get tabular data of states
    corresponding to params */
    this.getStates = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/location/state', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get tabular data of cities
    corresponding to params */
    this.getCities = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/location/city', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    this.unique = {};
    
    /* Get unique list of countries */
    this.unique.country = function(cb) {
        
        $http.post(CONFIG.host + '/api/v1/location/unique', { type: 'country' }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of state */
    this.unique.state = function(country, cb) {
        
        if (country === '' || country === undefined) {
            cb('error');
            return;
        }
        
        $http.post(CONFIG.host + '/api/v1/location/unique', { type: 'state', country: country }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of state */
    this.unique.city = function(country, state, cb) {
        
        if (country === '' || country === undefined || state === '' || state === undefined) {
            cb('error');
            return;
        }
        
        $http.post(CONFIG.host + '/api/v1/location/unique', { type: 'city', country: country, state: state }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    this.detail = {};
    /* Get details of state
    corresponding to id */
    this.detail.state = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/location/state', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    /* Get details of city
    corresponding to id */
    this.detail.city = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/location/city', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    this.put = {};
    /** Add/Edit State 
     */
    this.put.state = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/location/state', { data: data }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    /** Add/Edit City 
     */
    this.put.city = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/location/city', { data: data }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
});
