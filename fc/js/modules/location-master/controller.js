app.controller('locationViewController', function($scope, $location, locationService){
    
    $scope.pageHeader = 'Location Master';
    
    /* Variable to decide which buttons
    to show */
    $scope.show = {
        stateList: true,
        cityList: true
    };
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Country',
        key: 'country_name',
        sortable: true,
        type: 'string'
    }, {
        title: 'State',
        key: 'state_name',
        sortable: true,
        type: 'string'
    }, {
        title: 'City',
        key: 'city_name',
        sortable: true,
        type: 'string'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        locationService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    $scope.fetch();
    
    /* Function to redirect to
    add state/city mode */
    $scope.redirect = function(id) {
        $location.path('/location/' + id);
    };
    
});

app.controller('stateViewController', function($scope, $location, locationService){
    
    $scope.pageHeader = 'State Master';
    
    /* Variable to decide which buttons
    to show */
    $scope.show = {
        addState: true,
        back: true
    };
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Country',
        key: 'country_name',
        sortable: false,
        type: 'string'
    }, {
        title: 'State',
        key: 'state_name',
        sortable: true,
        type: 'string'
    }, {
        title: 'Edit',
        key: 'edit'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        locationService.getStates($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    $scope.fetch();
    
    /* Function to redirect to
    state add/edit mode */
    $scope.redirect = function(id) {
        
        if (id === 'back') {
            $location.path('/location');
            return;
        }
        
        $location.path('/location/state/' + id);
    };
    
});

app.controller('stateInputController', function($scope, $location, 
    $routeParams, locationService, toasterService) {
    
    /* Identify Page */
    $scope.screen = 'location-state';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {};
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        locationService.detail.state($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
        });
        
    }
    
    /* Location Operations */
    $scope.location = {
        get: {}
    };
    
    /* Get Country List */
    $scope.location.get.country = function() {
        locationService.unique.country(function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.country = response;
        });
    };
    $scope.location.get.country();
    
    /* State Operations */
    $scope.state = {};
    
    /* Save State Details */
    $scope.state.save = function() {
        
        $scope.errors = [];
        
        locationService.put.state($scope.o, function(err, response) {
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'State Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.state.cancel();
        });
    };
    
    /* Cancel State Add / Edit */
    $scope.state.cancel = function() {
        $location.path('/location/state');
    };
    
});

app.controller('cityViewController', function($scope, $location, locationService){
    
    $scope.pageHeader = 'City Master';
    
    /* Variable to decide which buttons
    to show */
    $scope.show = {
        addCity: true,
        back: true
    };
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Country',
        key: 'country_name',
        sortable: false,
        type: 'string'
    }, {
        title: 'State',
        key: 'state_name',
        sortable: false,
        type: 'string'
    }, {
        title: 'City',
        key: 'city',
        sortable: true,
        type: 'string'
    }, {
        title: 'Edit',
        key: 'edit'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        locationService.getCities($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    $scope.fetch();
    
    /* Function to redirect to
    add state/city mode */
    $scope.redirect = function(id) {
        
        if (id === 'back') {
            $location.path('/location');
            return;
        }
        
        $location.path('/location/city/' + id);
    };
    
});

app.controller('cityInputController', function($scope, $location, 
    $routeParams, locationService, toasterService) {
    
    /* Identify Page */
    $scope.screen = 'location-city';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {};
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        locationService.detail.city($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
        });
        
    }
    
    /* Location Operations */
    $scope.location = {
        get: {}
    };
    
    /* Get Country List */
    $scope.location.get.country = function() {
        locationService.unique.country(function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.country = response;
        });
    };
    $scope.location.get.country();
    
    /* Get State List */
    $scope.location.get.state = function(country) {
        locationService.unique.state(country, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.location.state = response;
        });
    };
    
    /* City Operations */
    $scope.city = {};
    
    /* Save City Details */
    $scope.city.save = function() {
        
        $scope.errors = [];
        
        locationService.put.city($scope.o, function(err, response) {
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'City Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.city.cancel();
        });
    };
    
    /* Cancel City Add / Edit */
    $scope.city.cancel = function() {
        $location.path('/location/city');
    };
    
    /* Watchers */
    $scope.$watch('o.country', function(n, o) {
        if (n === o) {
            return;
        }
        
        /* Change state if country was defined
        and is changed now */
        if (o !== undefined) {
            $scope.o.state = null;
        }

        $scope.location.get.state(n);
    });
    
});
