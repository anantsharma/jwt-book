app.controller('logStreamController', function($scope, $location) {
    
    $scope.pageHeader = 'Log Streaming';
    
    $scope.ipList = [
        '10.74.34.178',
        '10.74.34.179',
        '10.74.34.180',
        '10.74.34.181',
        '10.74.34.182'
    ];
    
});

app.directive('logBlock', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/partials/log-stream-master/log-block.html',
        scope: {
            ip: '='
        },
        controller: function($scope, $interval) {
            
            $scope.logs = [];
            
            var socket = io(`http://${$scope.ip}:26269`);
            
            socket.on('rOutput', function (data) {
                $scope.logs.push(data.msg);
            });
            
            socket.on('rError', function (data) {
                $scope.logs.push(data.msg);
            });
            
            socket.on('rExit', function (data) {
                $scope.logs.push(data.msg);
            });
            
            $interval(() => {
                console.log($scope.logs);
            }, 2000);
            
        }
    };
});



