app.controller('configController', function($scope, $location, configService) {
    
    $scope.pageHeader = 'Config';
    
    /* Function to fetch content */
    $scope.fetch = function() {
        configService.get(function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    $scope.fetch();
    
    /* Add new row */
    $scope.addRow = function() {
        $scope.data.push({});
    };
    
    /* Save/Update Record */
    $scope.save = function(obj) {
        
        obj.denomination = Number(obj.denomination);
        
        configService.put(obj, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.fetch();
            
        });
    };
    
    /* Delete Record */
    $scope.delete = function(obj) {
        
        configService.delete(obj, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.fetch();
            
        });
    };
    
});
