app.service('configService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(cb) {
        
        $http.get(CONFIG.host + '/api/v1/config').then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/config', {data: data}).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Delete data */
    this.delete = function(data, cb) {
        
        $http.delete(CONFIG.host + '/api/v1/config', {
            params: data
        }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
});
