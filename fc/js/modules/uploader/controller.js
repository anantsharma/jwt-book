app.controller('uploaderViewController', function($scope, $location, 
    uploaderService, Upload, $interval, $window, $timeout, toasterService) {
    
    $scope.pageHeader = 'Uploader';
    
    /** Initialize blank object
     * to contain information
     */
    $scope.o = {
        type: ''
    };
    $scope.uploadInProgress = false;
    
    /* Define table headers */
    $scope.headers = [{
        title: 'File',
        key: 'identifier',
        sortable: true,
        type: 'string'
    }, {
        title: 'File Name',
        key: 'fileName',
        sortable: true,
        type: 'string'
    }, {
        title: 'Start Time',
        key: 'start_ts',
        sortable: true,
        type: 'date',
        format: 'DD-MM-YYYY HH:mm:ss'
    }, {
        title: 'End Time',
        key: 'end_ts',
        sortable: true,
        type: 'date',
        format: 'DD-MM-YYYY HH:mm:ss'
    }, {
        title: 'Status',
        key: 'status',
        sortable: true,
        type: 'string'
    }, {
        title: 'Reason',
        key: 'info',
        sortable: true,
        type: 'number'
    }, {
        title: 'Action',
        key: 'ufcid'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        uploaderService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    $scope.fetch();
    
    /** Iteratively refresh
     * records 30 seconds
     */
    $scope.timer = $interval(function() {
        $scope.fetch();
    }, 30000);
    
    /** Destroy timer on 
     * state change
     */
    $scope.$on("$destroy", function() {
        if (angular.isDefined($scope.timer)) {
            $interval.cancel($scope.timer);
        }
    });
    
    /* Function to redirect to
    detail mode */
    $scope.redirect = function(id) {
        $location.path('/uploader/' + id);
    };
    
    /** Function to get
     * models against which
     * file will be uploaded for 
     * validation
     */
    uploaderService.getModels(function(err, response) {
        
        if (err) {
            console.trace(err);
            return;
        }
        
        $scope.models = response;
        
    });
    
    /** Upload file for
     * validation & processing
     */
    $scope.upload = function() {
        
        $scope.uploadInProgress = true;
        
        uploaderService.upload($scope.o, function(err, response) {
            
            $scope.uploadInProgress = false;
            $scope.o.file = null;
            
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'Uploader',
            };
            
            if (err) {
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            toastObj.body = 'File sent for processing';
            
            toasterService.pop(toastObj);
            
            $timeout(function() {
                $scope.fetch();
            }, 2000);
            
        });
        
    };
    
    /** Get Sample
     */
    $scope.getSample = function() {
        
        var e = '';
        
        switch ($scope.o.type.toUpperCase()) {
            case 'ATM MASTER': e = 'atm-master.xlsx'; break;
            case 'CASH BALANCE FILE': e = 'cash-balance-file.csv'; break;
            case 'CASHOUT DATA': e = 'cash-out-data.xlsx'; break;
            case 'COST DATA': e = 'cost-data.xlsx'; break;
            case 'CRA LOCATION': e = 'cra-location.xlsx'; break;
            case 'NODAL MASTER': e = 'nodal-branch.xlsx'; break;
            case 'UPTIME DATA': e = 'uptime-data.xlsx'; break;
            case 'ATM SERVICES DATA': e = 'atm-services-data.xlsx'; break;
            case 'HOPPER FILE': e = 'hopper-file.csv'; break;
            case 'HOLIDAY': e = 'holiday-file.csv'; break;
            default: e = 'atm-master.xlsx'; break;
        }
        
        $window.open(`./sample-files/${e}`);
        
    };
    
    /** Run Model from
     * UI for successful load of
     * cash balance file
     */
    $scope.runModel = function(id) {
        
        uploaderService.runModel(id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
        });
        
    };
    
});

app.controller('uploaderDetailController', function($scope, $location, 
    uploaderService, $routeParams, $timeout) {
        
    $scope.pageHeader = '';
    
    /** Redirect back to
     * uploader screen if
     * log id is not specified
     */
    if ($routeParams.id == undefined) {
        $location.path('/uploader');
        return;
    }
        
    /* Get Log Details */
    uploaderService.detail($routeParams.id, function(err, response) {
        if (err) {
            console.trace(err);
            return;
        }
        
        $scope.o = response;
        
    });
    
    /** Redirect back to
     * list view
     */
    $scope.redirect = function() {
        $location.path('/uploader/');
    };
    
    /** Set height of table
     */
    function setHeight() {
        
        var windowHeight = $(window).height();
        
        var marginHeight = $('.main-header').height() + $('.content-header').height();
        
        $('.uploader-detail-table').height(windowHeight - marginHeight - 100);
        
    }
    setHeight();
    
});
