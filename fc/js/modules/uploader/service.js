app.service('uploaderService', function($http, CONFIG, Upload, authService){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/uploader', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get details of log
    corresponding to fc/id sent as id */
    this.detail = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/uploader', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Upload File */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/uploader', { data: data }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /** Get file models
     * against which data needs to be uploaded
     */
    this.getModels = function(cb) {
        
        $http.get(CONFIG.validation + '/models').then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Upload File */
    this.upload = function(data, cb) {
        
        data.token = JSON.stringify(authService.parseJwt());
        
        Upload.upload({
            url: CONFIG.validation + '/upload',
            data: data
        }).then(function (response) {
            
            /** Fire callback on
             * successfull resolve
             */
            cb(null, response.data);
            
        }, function (response) {
            
            /** Fire err callback
             * on error
             */
            cb(response.data);
            
        }, function (event) {
            
            /** Progress event
             * Math.min is to fix IE which reports 200% sometimes
             */
            data.progress = Math.min(100, parseInt(100.0 * event.loaded / event.total, 10));
            
        });
        
    };
    
    /** Run Model from UI */
    this.runModel = function(id, cb) {
        
        $http.post(CONFIG.rModel + '/spawnR', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
});
