app.service('fIndentService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.post(CONFIG.host + '/api/v1/indent', params).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/indent', data).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
});
