app.controller('fIndentController', function($scope, $location, 
    fIndentService, nodalService, authService, $rootScope) {
    
    $scope.pageHeader = 'Indent';
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        bank: null,
        state: null,
        nodal_branch: null,
        cra_vendor: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        $scope.indentTree = [];
        
        fIndentService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.indentTree = response.aggregations.bank.buckets;
        });
    };
    
    /* Bank Operations */
    $scope.bank = {};
    
    /* Location Operations */
    $scope.location = {};
    
    /* Nodal Branch Operations */
    $scope.nodalBranch = {
        get: {}
    };
    /* Nodal Branch List */
    $scope.nodalBranch.get.list = function() {
        nodalService.unique($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.nodalBranch.list = response;
        });
    };
    
    /* Function to populate
    permission based values */
    $scope.evaluateToken = function() {
        
        /* Get Token */
        var token = authService.parseJwt();
        
        /* Assign Permissions */
        $scope.bank.list = token.permissions.banks;
        $scope.location.state = token.permissions.states;
        
        /* Insert Empty Values */
        $scope.bank.list.push('');
        $scope.location.state.push('');
        
    };
    $scope.evaluateToken();
    
    $rootScope.$on('authService.token.save', function(event, data) {
        $scope.evaluateToken();
    });
    
    /* Watchers */
    $scope.$watchGroup(['params.bank', 'params.state'], function(n, o) {
        
        if (n === o) {
            return;
        }
        
        $scope.params.nodal_branch = '';
        $scope.nodalBranch.list = [];
        
        if ($scope.params.bank === '' || $scope.params.state === '') {
            return;
        }
        
        $scope.nodalBranch.get.list();
    });
    
    /* Export */
    $scope.export = function() {
        
        fIndentService.put($scope.indentTree, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
        });
    };
    
});
