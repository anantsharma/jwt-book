app.controller('commonsViewController', function($scope, $location, commonsService){
    
    $scope.pageHeader = 'Commons Master';
    
    /* Define table headers */
    $scope.headers = [{
        title: 'Master Name',
        key: 'master',
        sortable: true,
        type: 'string'
    },{
        title: 'Value',
        key: 'value',
        sortable: true,
        type: 'string'
    },{
        title: 'Edit',
        key: 'edit'
    },{
        title: 'Status',
        key: 'active'
    }];
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        page: 1,
        q: null,
        sort: null,
        order: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        commonsService.get($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.data = response;
        });
    };
    
    $scope.fetch();
    
    /* Function to redirect to
    add/edit mode */
    $scope.redirect = function(id) {
        $location.path('/commons/' + id);
    };
    
    /* Enable/Disable */
    $scope.toggle = function(obj) {
        var o = {};
        o["fc/id"] = obj["fc/id"];
        o.active = obj.active;
        
        commonsService.put(o, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
        });
        
    };
    
});

app.controller('commonsInputController', function($scope, $location, 
    $routeParams, commonsService, toasterService){
    
    /* Identify Page */
    $scope.screen = 'commons';
    
    /* Initialize blank obj 
    to contain information */
    $scope.o = {};
    
    /* Based on id, determines
    whether the view is in add or 
    edit mode. Defaults to add mode */
    $scope.mode = 'add';
    
    if ($routeParams.id != undefined) {
        $scope.mode = 'edit';
        
        /* Get Details */
        commonsService.detail($routeParams.id, function(err, response) {
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.o = response;
            
        });
        
    }
    
    /* Commons Operations */
    $scope.commons = {};
    
    /* List of unique Masters */
    commonsService.unique.master(function(err, response) {
            
        if (err) {
            console.trace(err);
            return;
        }
        
        $scope.commons.list = response;
    });
    
    /* Save Details */
    $scope.commons.save = function() {
        
        $scope.errors = [];
        
        commonsService.put($scope.o, function(err, response) {
            
            /** Prepare toast object
             */
            var toastObj = {
                type: 'success',
                title: 'Commons Master',
            };
            
            if (err) {
                
                if (err.hasOwnProperty('errors')) {
                    $scope.errors = err.errors;
                    return;
                }
                
                toastObj.type = 'error';
                toastObj.body = 'Some error occurred';
                toasterService.pop(toastObj);
                
                return;
            }
            
            if ($scope.mode === 'add') {
                toastObj.body = 'Record added successfully';
            } else {
                toastObj.body = 'Record updated successfully';
            }
            
            toasterService.pop(toastObj);
            $scope.commons.cancel();
        });
    };
    
    /* Cancel Add / Edit */
    $scope.commons.cancel = function() {
        $location.path('/commons');
    };
    
});
