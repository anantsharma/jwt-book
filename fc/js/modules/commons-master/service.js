app.service('commonsService', function($http, CONFIG){
    var self = this;
    
    /* Get tabular data corresponding 
    to params */
    this.get = function(params, cb) {
        
        if (typeof(params) === 'object' && !Array.isArray(params)) {
            $http.get(CONFIG.host + '/api/v1/common-master', { params: params }).then(function(response) {
                cb(null, response.data);
            });
        }
        
    };
    
    /* Get details corresponding 
    to fc/id sent as id */
    this.detail = function(id, cb) {
        
        $http.post(CONFIG.host + '/api/v1/common-master', { id: id }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    this.unique = {};
    
    /* Get unique list of masters */
    this.unique.master = function(cb) {
        
        $http.post(CONFIG.host + '/api/v1/common-master/unique', { type: '' }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of values
    corresponding to master */
    this.unique.value = function(master, cb) {
        
        if (master === '' || master === undefined) {
            cb('error');
            return;
        }
        
        $http.post(CONFIG.host + '/api/v1/common-master/unique', { type: master }).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/common-master', { data: data }).then(function(response) {
            cb(null, response.data);
        }, function(response) {
            cb(response.data);
        });
        
    };
    
});
