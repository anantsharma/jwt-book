app.controller('rIndentController', function($scope, $location, 
    fIndentService, nodalService, authService, $rootScope, rIndentService) {
    
    $scope.pageHeader = 'Revised Indent';
    
    /* Initialize parameters to 
    be sent along with query */
    $scope.params = {
        bank: null,
        state: null,
        nodal_branch: null,
        cra_vendor: null,
        id: null
    };
    
    /* Function to fetch content
    by passing params */
    $scope.fetch = function() {
        $scope.indent.data = {};
        
        rIndentService.fetch($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            response.revisedIndentTotalAmount = response.indentTotalAmount;
            response.atms.forEach(function(atm) {
                atm.revisedTotalAmount = atm.totalAmount;
                
                atm.rCassettes = angular.copy(atm.cassettes);
            });
            $scope.indent.data = response;
        });
    };
    
    /* Bank Operations */
    $scope.bank = {};
    
    /* Location Operations */
    $scope.location = {};
    
    /* Nodal Branch Operations */
    $scope.nodalBranch = {
        get: {}
    };
    /* Nodal Branch List */
    $scope.nodalBranch.get.list = function() {
        nodalService.unique($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.nodalBranch.list = response;
        });
    };
    
    /* Function to populate
    permission based values */
    $scope.evaluateToken = function() {
        
        /* Get Token */
        var token = authService.parseJwt();
        
        /* Assign Permissions */
        $scope.bank.list = token.permissions.banks;
        $scope.location.state = token.permissions.states;
        
        /* Insert Empty Values */
        $scope.bank.list.push('');
        $scope.location.state.push('');
        
    };
    $scope.evaluateToken();
    
    /* Indent Operations */
    $scope.indent = {
        get: {}
    };
    $scope.indent.get.list = function() {
        $scope.indent.list = [];
        
        rIndentService.unique($scope.params, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
            $scope.indent.list = response;
        });
    };
    $scope.indent.get.list();
    
    $rootScope.$on('authService.token.save', function(event, data) {
        $scope.evaluateToken();
    });
    
    /* Watchers */
    $scope.$watchGroup(['params.bank', 'params.state'], function(n, o) {
        
        if (n === o) {
            return;
        }
        
        $scope.params.nodal_branch = '';
        $scope.nodalBranch.list = [];
        
        if ($scope.params.bank === '' || $scope.params.state === '') {
            return;
        }
        
        $scope.nodalBranch.get.list();
    });
    
    $scope.$watchGroup(['params.bank', 'params.state', 'params.nodal_branch', 'params.cra_vendor'], function(n, o) {
        
        if (n === o) {
            return;
        }
        
        $scope.params.id = '';
        $scope.indent.list = [];
        
        $scope.indent.get.list();
    });
    
    /* Export */
    $scope.export = function() {
        
        rIndentService.put($scope.indent.data, function(err, response) {
            
            if (err) {
                console.trace(err);
                return;
            }
            
        });
    };
    
});
