app.service('rIndentService', function($http, CONFIG){
    var self = this;
    
    /* Fetch indent corresponding
    to indent order id */
    this.fetch = function(params, cb) {
        
        $http.post(CONFIG.host + '/api/v1/indent/fetch', params).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Get unique list of indents
    filtered by the passed params */
    this.unique = function(params, cb) {
        
        $http.post(CONFIG.host + '/api/v1/indent/unique', params).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
    /* Insert data */
    this.put = function(data, cb) {
        
        $http.put(CONFIG.host + '/api/v1/indent/r', data).then(function(response) {
            cb(null, response.data);
        });
        
    };
    
});
