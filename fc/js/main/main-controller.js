app.controller('mainController', function($scope, authService, $location, toasterService) {
    
    if (!authService.isAuthed()) {
        $location.path('/login');
    } else {
        authService.refreshToken();
    }
    
});