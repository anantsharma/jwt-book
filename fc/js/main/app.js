'use strict';

var app = angular.module('fc', ['ngRoute', 'ngMaterial', 'ui.bootstrap',
    'ui.select', 'ngSanitize', 'ngFileUpload', 'toaster', 'ngAnimate']);
