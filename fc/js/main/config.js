'use strict';

app.constant('CONFIG', {
    host: 'http://10.74.34.178:26231',
    validation: 'http://10.74.34.178:26270',
    rModel: 'http://10.74.34.178:26269'
});

app.run(function($rootScope, $location, authService) {
    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        
        if ($location.path() !== '/login') {
            
            if (!authService.isAuthed()) {
                $location.path('/login');
                return;
            }
            
            $('.main-header').show();
            $('.main-sidebar').show();
        }
        
        function fixLayout() {
            try {
                angular.element(function () {
                    $.AdminLTE.layout.fix();
                });
            } catch (e) {
                fixLayout();
            }
        }
        fixLayout();
        
    });
});

app.config(function($routeProvider, $httpProvider){
	
	$routeProvider
	
	    /* Bank Master Configuration */
        .when('/bank', {
            templateUrl: "fc/partials/bank-master/view.html",
            controller: 'bankViewController',
        })
        .when('/bank/new', {
            templateUrl: "fc/partials/bank-master/input.html",
            controller: 'bankInputController',
        })
        .when('/bank/:id', {
            templateUrl: "fc/partials/bank-master/input.html",
            controller: 'bankInputController',
        })
        
        /* Nodal Master Configuration */
        .when('/nodal', {
            templateUrl: "fc/partials/nodal-master/view.html",
            controller: 'nodalViewController',
        })
        .when('/nodal/new', {
            templateUrl: "fc/partials/nodal-master/input.html",
            controller: 'nodalInputController',
        })
        .when('/nodal/:id', {
            templateUrl: "fc/partials/nodal-master/input.html",
            controller: 'nodalInputController',
        })
        
        /* CIT Master Configuration */
        .when('/cit', {
            templateUrl: "fc/partials/cit-master/view.html",
            controller: 'citViewController',
        })
        .when('/cit/new', {
            templateUrl: "fc/partials/cit-master/input.html",
            controller: 'citInputController',
        })
        .when('/cit/:id', {
            templateUrl: "fc/partials/cit-master/input.html",
            controller: 'citInputController',
        })
        
        /* CRA Location Master Configuration */
        .when('/cra-location', {
            templateUrl: "fc/partials/cra-location-master/view.html",
            controller: 'craLocationViewController',
        })
        .when('/cra-location/new', {
            templateUrl: "fc/partials/cra-location-master/input.html",
            controller: 'craLocationInputController',
        })
        .when('/cra-location/:id', {
            templateUrl: "fc/partials/cra-location-master/input.html",
            controller: 'craLocationInputController',
        })
        
        /* ATM Master Configuration */
        .when('/atm', {
            templateUrl: "fc/partials/atm-master/view.html",
            controller: 'atmViewController',
        })
        .when('/atm/new', {
            templateUrl: "fc/partials/atm-master/input.html",
            controller: 'atmInputController',
        })
        .when('/atm/:id', {
            templateUrl: "fc/partials/atm-master/input.html",
            controller: 'atmInputController',
        })
        
        /* Project Master Configuration */
        .when('/project', {
            templateUrl: "fc/partials/project-master/view.html",
            controller: 'projectViewController',
        })
        .when('/project/new', {
            templateUrl: "fc/partials/project-master/input.html",
            controller: 'projectInputController',
        })
        .when('/project/:id', {
            templateUrl: "fc/partials/project-master/input.html",
            controller: 'projectInputController'
        })
        
        /* Commons Master Configuration */
        .when('/commons', {
            templateUrl: "fc/partials/commons-master/view.html",
            controller: 'commonsViewController'
        })
        .when('/commons/new', {
            templateUrl: "fc/partials/commons-master/input.html",
            controller: 'commonsInputController'
        })
        .when('/commons/:id', {
            templateUrl: "fc/partials/commons-master/input.html",
            controller: 'commonsInputController'
        })
        
        /* Rule Master Configuration */
        .when('/rule', {
            templateUrl: "fc/partials/rule-master/view.html",
            controller: 'ruleViewController'
        })
        .when('/rule/new', {
            templateUrl: "fc/partials/rule-master/input.html",
            controller: 'ruleInputController'
        })
        .when('/rule/:id', {
            templateUrl: "fc/partials/rule-master/input.html",
            controller: 'ruleInputController'
        })
        
        /* User Master Configuration */
        .when('/user', {
            templateUrl: "fc/partials/user-master/view.html",
            controller: 'userViewController'
        })
        .when('/user/new', {
            templateUrl: "fc/partials/user-master/input.html",
            controller: 'userInputController'
        })
        .when('/user/:id', {
            templateUrl: "fc/partials/user-master/input.html",
            controller: 'userInputController'
        })
        
        /* Cash Planner Configuration */
        .when('/planner', {
            templateUrl: "fc/partials/cash-planner/view.html",
            controller: 'plannerController'
        })
        
        /* First Indent Configuration */
        .when('/findent', {
            templateUrl: "fc/partials/f-indent/view.html",
            controller: 'fIndentController'
        })
        
        /* Revised Indent Configuration */
        .when('/rindent', {
            templateUrl: "fc/partials/r-indent/view.html",
            controller: 'rIndentController'
        })
        
        /* Config Master Configuration */
        .when('/config', {
            templateUrl: "fc/partials/config-master/view.html",
            controller: 'configController'
        })
        
        /* Login Master Configuration */
        .when('/login', {
            templateUrl: "fc/partials/login/login.html",
            controller: 'loginController'
        })
        
        /* Location Master Configuration */
        .when('/location', {
            templateUrl: "fc/partials/location-master/view.html",
            controller: 'locationViewController'
        })
        /* State Configuration */
        .when('/location/state', {
            templateUrl: "fc/partials/location-master/view.html",
            controller: 'stateViewController'
        })
        .when('/location/state/new', {
            templateUrl: "fc/partials/location-master/state-input.html",
            controller: 'stateInputController'
        })
        .when('/location/state/:id', {
            templateUrl: "fc/partials/location-master/state-input.html",
            controller: 'stateInputController'
        })
        /* City Configuration */
        .when('/location/city', {
            templateUrl: "fc/partials/location-master/view.html",
            controller: 'cityViewController'
        })
        .when('/location/city/new', {
            templateUrl: "fc/partials/location-master/city-input.html",
            controller: 'cityInputController'
        })
        .when('/location/city/:id', {
            templateUrl: "fc/partials/location-master/city-input.html",
            controller: 'cityInputController'
        })
        
        /* Uploader */
        .when('/uploader', {
            templateUrl: "fc/partials/uploader/view.html",
            controller: 'uploaderViewController'
        })
        .when('/uploader/:id', {
            templateUrl: "fc/partials/uploader/detail.html",
            controller: 'uploaderDetailController'
        })
        
        /* Log Stream Master Configuration */
        .when('/stream', {
            templateUrl: "fc/partials/log-stream-master/view.html",
            controller: 'logStreamController'
        })
        
        .otherwise({
            redirectTo: '/login'
        });
        
    $httpProvider.interceptors.push('authInterceptor');
});
