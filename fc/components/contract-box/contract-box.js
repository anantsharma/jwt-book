app.directive('fcContractBox', function(){
    return{
        restrict: 'E',
        templateUrl: 'fc/components/contract-box/contract-box.html',
        scope: {
            c: '=',
            remove: '&',
            screen: '=?'
        },
        controller: function($scope) {
            
        }
    };
});
