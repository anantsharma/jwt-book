app.directive('leftSidebarContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/left-sidebar/left-sidebar-container.html',
        controller: function($scope, $location, authService, $rootScope) {
            
            $scope.masters = [{
                name: 'Bank Master',
                screen_name: 'Bank Master',
                url: '/bank',
                active: true,
                show: false
            }, {
                name: 'Nodal Branch Master',
                screen_name: 'Nodal Branch Master',
                url: '/nodal',
                active: false,
                show: false
            }, {
                name: 'CIT Master',
                screen_name: 'CIT Master',
                url: '/cit',
                active: false,
                show: false
            }, {
                name: 'CRA Location Master',
                screen_name: 'CRA Location Master',
                url: '/cra-location',
                active: false,
                show: false
            }, {
                name: 'Commons Master',
                screen_name: 'Common Master',
                url: '/commons',
                active: false,
                show: false
            }, {
                name: 'Project Master',
                screen_name: 'Project Master',
                url: '/project',
                active: false,
                show: false
            }, {
                name: 'Rules Master',
                screen_name: 'Rules Master',
                url: '/rule',
                active: false,
                show: false
            }, {
                name: 'ATM Master',
                screen_name: 'ATM Master',
                url: '/atm',
                active: false,
                show: false
            }, {
                name: 'Location Master',
                screen_name: 'Location Master',
                url: '/location',
                active: false,
                show: false,
            }];
            $scope.navList = [{
                name: 'ATM Cash Planning',
                screen_name: 'Cash Planner',
                url: '/planner',
                active: false,
                show: false,
                icon: 'fa-credit-card'
            }, {
                name: 'Indent',
                screen_name: 'Indent Screen',
                url: '/findent',
                active: false,
                show: false,
                icon: 'fa-indent'
            }, {
                name: 'Revised Indent',
                screen_name: 'Indent Screen',
                url: '/rindent',
                active: false,
                show: false,
                icon: 'fa-indent'
            }, {
                name: 'User Management',
                screen_name: 'User Management',
                url: '/user',
                active: false,
                show: false,
                icon: 'fa-user'
            }, {
                name: 'Uploader',
                screen_name: 'Uploader Screen',
                url: '/uploader',
                active: false,
                show: false,
                icon: 'fa-upload'
            }];
            
            /* Function for navigation */
            $scope.navigate = function(obj) {
                
                /* Set all actives as false */
                angular.forEach($scope.masters, function(o) {
                    o.active = false;
                });
                angular.forEach($scope.navList, function(o) {
                    o.active = false;
                });
                
                /* Set active for clicked item */
                obj.active = true;
                
                $location.path(obj.url);
            };
            
            /* Function to select screens */
            $scope.evaluate = function() {
                
                if (!authService.isAuthed()) {
                    return;
                }
                
                /* Get Token */
                var token = authService.parseJwt();
                
                try {
                    var screenArr = token.permissions.screens;
                    
                    $scope.masters.forEach(function(obj) {
                        
                        obj.show = false;
                        
                        if (screenArr.indexOf(obj.screen_name) > -1) {
                            obj.show = true;
                        }
                        
                    });
                    
                    $scope.navList.forEach(function(obj) {
                        
                        obj.show = false;
                        
                        if (screenArr.indexOf(obj.screen_name) > -1) {
                            obj.show = true;
                        }
                        
                    });
                    
                } catch (e) {
                    console.trace(e);
                }
                
            };
            $scope.evaluate();
            
            $rootScope.$on('authService.token.save', function(event, data) {
                $scope.evaluate();
            });
            
        }
    };
});
