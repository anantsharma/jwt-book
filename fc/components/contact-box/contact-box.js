app.directive('fcContactBox', function(){
    return{
        restrict: 'E',
        templateUrl: 'fc/components/contact-box/contact-box.html',
        scope: {
            c: '=',
            remove: '&',
            screen: '=?'
        },
        controller: function($scope) {
            
        }
    };
});
