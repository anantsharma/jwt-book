app.directive('navbarContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/navbar/navbar-container.html',
        scope: {},
        controller: function($scope, authService, $rootScope, userService, toasterService) {
            
            $scope.user = {};
            $scope.o = {};
            
            $scope.evaluate = function() {
                if (authService.isAuthed()) {
                    $scope.user = authService.parseJwt();
                }
            };
            $scope.evaluate();
            
            $rootScope.$on('authService.token.save', function(event, data) {
                $scope.evaluate();
            });
            
            $scope.logout = function() {
                authService.logout();
            };
            
            /* Trigger change password */
            $scope.changePassword = function() {
                
                $scope.errors = [];
                
                userService.changePassword($scope.o, function(err, response) {
                    
                    /** Prepare toast object
                     */
                    var toastObj = {
                        type: 'success',
                        title: 'Password Change'
                    };
                    
                    if (err) {
                        
                        if (err.status === 'error') {
                            $scope.errors.push(err.msg);
                        } else {
                            toastObj.type = 'error';
                            toastObj.body = 'Some error occurred';
                            toasterService.pop(toastObj);
                        }
                        
                        return;
                    }
                    
                    toastObj.body = response.body;
                    toasterService.pop(toastObj);
                    
                });
                
            };
        }
    };
});
