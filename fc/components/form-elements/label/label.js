app.directive('fcLabel', function(utils) {
    return {
        restrict: 'AEC',
        templateUrl: 'fc/components/form-elements/label/label.html'
    };
});
