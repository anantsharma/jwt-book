app.directive('fcSwitch', function(){
    return{
        restrict: 'AEC',
        templateUrl: 'fc/components/form-elements/switch/switch.html',
        scope: {
            label: '=',
            model: '='
        },
        controller: function($scope, utils) {
            
            /* Process value for boolean */
            $scope.model = utils.processBool($scope.model);
            
        }
    };
});
