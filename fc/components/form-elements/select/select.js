app.directive('fcSelect', function(utils, $location){
    return {
        restrict: 'AEC',
        templateUrl: 'fc/components/form-elements/select/select.html',
        scope: {
            label: '=',
            model: '=',
            list: '=?',
            screen: '=?',
            mode: '=?'
        },
        link: function(scope, element, attrs, controller) {
            
            /* Modify label if required parameter
            is present in attrs */
            if (attrs.hasOwnProperty('required')) {
                scope.required = true;
            }
            
            /** Diable on edit mode */
            if (scope.mode === 'edit' && attrs.hasOwnProperty('editdisabled')) {
                scope.isDisabled = true;
            }
            
        },
        controller: function($scope, commonsService, citService,
        atmService, projectService) {
            
            $scope.selected = {};
            
            $scope.$watch('model', function(n, o) {
                
                if (n === '') {
                    $scope.selected.value = null;
                    return;
                }
                
                $scope.selected.value = utils.upperCase(n);
                
            });
            
            $scope.$watch('list', function(n, o) {
                if (n === o) {
                    return;
                }
                
                if (n.indexOf('') === -1) {
                    n.push('');
                }
                
                $scope.selected.value = $scope.model;
            });
            
            $scope.change = function() {
                $scope.model = $scope.selected.value;
            };
            
            /* Define conditional drop down constants */
            
            /* CIT agency */
            if ($scope.label === 'CIT Agency' || $scope.label === 'CRA') {
                citService.unique(function(err, response) {
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.list = response;
                });
            }
            
            /* Escalation level */
            if ($scope.label === 'Escalation Level') {
                $scope.list = [1, 2, 3, 4, 5];
            }
            
            /* User Master Roles */
            if ($scope.label === 'Role') {
                $scope.list = ['Admin', 'Manager', 'Planner'];
            }
            
            /* LHO/ZO */
            if ($scope.label === 'LHO/ZO') {
                atmService.unique({ key: 'lho_zo' }, function(err, response) {
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.list = response;
                });
            }
            
            /* Project */
            if ($scope.label === 'Project') {
                
                /** If label is Project and 
                 * screen is ATM Master Input, then
                 * projects are fetched from Project Master
                 */
                if ($scope.screen === 'atm-master-input') {
                    projectService.unique(null, function(err, response) {
                        if (err) {
                            console.trace(err);
                            return;
                        }
                        
                        $scope.list = response;
                    });
                } else {
                    atmService.unique({ key: 'project' }, function(err, response) {
                        if (err) {
                            console.trace(err);
                            return;
                        }
                        
                        $scope.list = response;
                    });
                }
                
            }
            
            /* Commons Master */
            var commonMasterArray = ["ATM Model", "ATM Chest Lock", "Region",
                "ATM Make", "Location Class", "Site Class", "Site Sub Class",
                "Zone", "Department", "Expense", "Switch Name", "OS Version",
                "VIP Status", "Cash Load Method", "Contract Type", "EJ Software",
                "ATM Chest Lock Status"];
            if (commonMasterArray.indexOf($scope.label) > -1) {
                commonsService.unique.value($scope.label, function(err, response) {
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.list = response;
                });
            }
            
            if ($scope.label === 'Loading Method') {
                commonsService.unique.value('Cash Load Method', function(err, response) {
                    if (err) {
                        console.trace(err);
                        return;
                    }
                    
                    $scope.list = response;
                });
            }
            
        }
    };
});
