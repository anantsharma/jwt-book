app.directive('revisedIndentContainer', function(cassetteService){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/indent/revised-indent-container.html',
        scope: {
            indent: '='
        },
        controller: function($scope) {
            
            $scope.splitIndentAmount = function() {
                
                var ratio = 0;
                
                /* Ratio Calculation */
                if ($scope.indent.indentTotalAmount && 
                    $scope.indent.indentTotalAmount != 0 &&
                    $scope.indent.revisedIndentTotalAmount && 
                    $scope.indent.revisedIndentTotalAmount != '') {
                    ratio = $scope.indent.revisedIndentTotalAmount / $scope.indent.indentTotalAmount;
                }
                
                /* Modify ATM Total Amount */
                angular.forEach($scope.indent.atms, function(atm) {
                    
                    /* Calculate revised amount for each atm */
                    atm.revisedTotalAmount = parseInt(ratio * atm.totalAmount, 10);
                    
                    $scope.splitATMAmount(atm);
                    
                });
                
            };
            
            $scope.splitATMAmount = function(atm) {
                
                cassetteService.fill(atm.revisedTotalAmount,
                    atm.rCassettes, atm.contractType, 'rindent');
                
            };
            
            $scope.updateTotal = function(atm) {
                
                atm.revisedTotalAmount = 0;
                
                angular.forEach(atm.rCassettes, function(c) {
                    
                    atm.revisedTotalAmount += c.finalNotes * c.denomination;
                    
                });
                
            };
            
        }
    };
});
