app.directive('indentNodalContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/indent/indent-nodal-container.html'
    };
});
