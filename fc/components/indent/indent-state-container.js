app.directive('indentStateContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/indent/indent-state-container.html'
    };
});