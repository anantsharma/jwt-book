app.directive('indentBankContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/indent/indent-bank-container.html'
    };
});
