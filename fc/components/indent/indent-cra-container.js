app.directive('indentCraContainer', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/indent/indent-cra-container.html',
        scope: {
            cra: '='
        },
        controller: function($scope) {
            
            $scope.indentTotalAmount = 0;
            
            /* Traverse through each ATM for computation */
            angular.forEach($scope.cra.atm_id.buckets, function(atm) {
                
                atm.totalAmount = 0;
                
                /* Calculate ATM total amount */
                angular.forEach(atm.cassettes, function(c) {
                    atm.totalAmount += c.denomination * c.finalNotes;
                });
                
                $scope.indentTotalAmount += atm.totalAmount;
                
            });
            
        }
    };
});
