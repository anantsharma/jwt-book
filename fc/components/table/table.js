app.directive('tableContainer', function(){
    return{
        restrict: 'E',
        templateUrl: 'fc/components/table/table-container.html',
        scope: {
            headers: '=',
            data: '=',
            sortFn: '&',
            params: '=',
            pageChange: '&',
            search: '&',
            redirect: '&',
            toggle: '&?',
            runModel: '&?'
        },
        controller: function($scope, utils){
            
            /* Watcher function to 
            process data as it changes */
            $scope.$watch('data', function(n, o){
                
                if(n===undefined){
                    return;
                }
                
                angular.forEach($scope.data.hits, function(obj){
                    try{
                        obj.active = utils.processBool(obj.active);
                    }catch(e){
                        obj.active = false;
                        console.trace(e);
                    }
                });
            });
            
            /* Function to fetch data
            based on sorting */
            $scope.processSorting = function(header) {
                if(!header.sortable){
                    return;
                }
                
                if($scope.params.sort === header.key){
                    if($scope.params.order === 'asc'){
                        $scope.params.order = 'desc';
                    }else{
                        $scope.params.order = 'asc';
                    }
                }else{
                    $scope.params.order = 'asc';
                }
                
                $scope.params.sort = header.key;
                $scope.sortFn();
            };
            
            /* Function to process date */
            $scope.formatDate = function(hObj, value) {
                
                if (!value || value == '') {
                    return '';
                }
                
                var format = hObj.format || 'DD-MM-YYYY';
                return moment(value).format(format);
                
            };
            
            $scope.modelRunning = false;
            
        }
    };
});
