app.directive('fcErrorBox', function(){
    return {
        restrict: 'E',
        templateUrl: 'fc/components/error-box/error-box.html',
    };
});
